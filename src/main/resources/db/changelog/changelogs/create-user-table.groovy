package db.changelog.changelogs


databaseChangeLog
        {
            changeSet(author: "LkMa", id:"create-user-table")
                    {
                        createTable(tableName:"user")
                                {
                                    column(name:"user_id", type: "Int", autoIncrement: true)
                                            {
                                                constraints(nullable: false, primaryKey: true)
                                            }
                                    column(name:"username", type: "varchar(50)")
                                            {
                                                constraints(nullable: false, unique: true)
                                            }
                                    column(name:"email", type: "varchar(50)")
                                            {
                                                constraints(nullable: false, unique: true)
                                            }
                                    column(name:"provider", type:"varchar(45)")
                                            {
                                                constraints(nullable: false)
                                            }
                                    column(name:"provider_id", type:"Int")
                                    column(name:"creation_timestamp", type: "timestamp", defaultValue:"now()")
                                    column(name:"deletion_timestamp", type: "timestamp")                                }

                    }
        }