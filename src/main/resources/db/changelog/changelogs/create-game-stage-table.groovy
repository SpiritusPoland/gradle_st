package db.changelog.changelogs


databaseChangeLog
        {
            changeSet(author: "LkMa", id: "create-game-stage-table")
                    {
                        createTable(tableName: "game_stage")
                                {
                                    column(name: "game_stage_id", type: "Int", autoIncrement: true)
                                            {
                                                constraints(nullable: false, primaryKey: true)
                                            }
                                    column(name: "start_timestamp", type: "timestamp", defaultValue:"now()")
                                    column(name: "end_timestamp", type: "timestamp")
                                    column(name: "game_id", type: "int")
                                            {
                                                constraints(nullable: false, foreignKeyName: "game_id_FK", references: "game(game_id)")
                                            }
                                    column(name: "child_game_stage", type: "int")
                                            {
                                                constraints(nullable: false, foreignKeyName: "child_game_stage_FK", references: "game_stage(game_stage_id)")
                                            }
                                    column(name: "parent_game_stage", type: "int")
                                            {
                                                constraints(nullable: false, foreignKeyName: "parent_game_stage_FK", references: "game_stage(game_stage_id)")
                                            }
                                    column(name: "base_stage", type: "int")
                                            {
                                                constraints(nullable: false, foreignKeyName: "base_stage_FK", references: "stage(stage_id)")
                                            }

                                }
                    }
        }