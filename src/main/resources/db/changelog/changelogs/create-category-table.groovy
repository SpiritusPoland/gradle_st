package db.changelog.changelogs


databaseChangeLog
        {
            changeSet(author: "LkMa", id:"create-category-table")
                    {
                        createTable(tableName:"category")
                                {
                                    column(name: "category_id", type: "Int", autoIncrement: true)
                                            {
                                                constraints(nullable: false, primaryKey: true )
                                            }
                                    column(name: "name", type: "varchar(80)")
                                }
                    }
        }