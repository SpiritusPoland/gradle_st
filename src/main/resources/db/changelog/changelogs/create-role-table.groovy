package db.changelog.changelogs

databaseChangeLog(logicalFilePath: '')
        {
            changeSet(author: "LkMa", id:"create-role-table")
                    {
                        createTable(tableName:"role")
                                {
                                    column(name: "role_id", type: "Int", autoIncrement: true)
                                            {
                                                constraints(nullable: false, primaryKey: true)
                                            }
                                    column(name: "role_name", type: "varchar(50)")
                                            {
                                                constraints(nullable: false, unique: true)
                                            }
                                }

                    }
        }