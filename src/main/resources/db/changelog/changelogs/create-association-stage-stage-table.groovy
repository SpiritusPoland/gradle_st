
databaseChangeLog
        {
            changeSet(author: 'LkMa', id:'create-association-stage-stage-category-table')
                    {
                        createTable(tableName:'stage_stage')
                                {
                                    column(name: 'parent_stage_id', type: 'Int')
                                            {
                                                constraints(nullable: false, foreignKeyName:'parent_stage_id_FK', references: 'stage(stage_id)' )
                                            }
                                    column(name: 'child_stage_id', type: 'Int')
                                            {
                                                constraints(nullable: false,  foreignKeyName: 'child_stage_id_FK', references: 'stage(stage_id)')
                                            }
                                }
                        addPrimaryKey(tableName: 'stage_stage', columnNames: 'parent_stage_id, child_stage_id', constraintName:'PK_stage_stage')
                    }
        }