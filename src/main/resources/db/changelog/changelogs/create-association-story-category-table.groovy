package db.changelog.changelogs


databaseChangeLog
        {
            changeSet(author: "LkMa", id:"create-association-story-category-table")
                    {
                        createTable(tableName:"story_category")
                                {
                                    column(name: "story_id", type: "Int")
                                            {
                                                constraints(nullable: false, foreignKeyName:"story_id_FK", references: "story(story_id)" )
                                            }
                                    column(name: "category_id", type: "Int")
                                            {
                                                constraints(nullable: false,  foreignKeyName: "category_id_FK", references: "category(category_id)")
                                            }
                                }
                        addPrimaryKey(tableName: "story_category", columnNames: "story_id, category_id", constraintName:"PK_story_category")
                    }
        }