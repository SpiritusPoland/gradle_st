package db.changelog.changelogs


databaseChangeLog
        {
            changeSet(author: "LkMa", id: "create-game-table")
                    {
                        createTable(tableName: "game")
                                {
                                    column(name: "game_id", type: "Int", autoIncrement: true)
                                            {
                                                constraints(nullable: false, primaryKey: true)
                                            }
                                    column(name: "story_id", type: "int")
                                            {
                                                constraints(nullable: false, foreignKeyName: "game_story_id_FK", references: "story(story_id)")
                                            }
                                    column(name: "user_id", type: "int")
                                            {
                                                constraints(nullable: false, foreignKeyName: "player_id_FK", references: "user(user_id)")
                                            }
                                    column(name: "start_timestamp", type: "timestamp", defaultValue: "now()")
                                    column(name: "end_timestamp", type: "timestamp")

                                }
                    }
        }