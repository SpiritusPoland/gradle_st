package db.changelog.changelogs


databaseChangeLog
        {
            changeSet(author: "LkMa", id: "create-rating-table")
                    {
                        createTable(tableName: "rating")
                                {
                                    column(name: "rating_id", type: "Int", autoIncrement: true)
                                            {
                                                constraints(nullable: false, primaryKey: true)
                                            }
                                    column(name: "rating_score", type: "float(4,2)")
                                            {
                                                constraints(nullable: false)
                                            }
                                    column(name: "user_id", type: "int")
                                            {
                                                constraints(foreignKeyName: "userRateing_id_FK", references: "user(user_id)")
                                            }
                                    column(name: "story_id", type: "int")
                                            {
                                                constraints(foreignKeyName: "storyRateing_id_FK", references: "story(story_id)")
                                            }
                                    column(name: "description", type: "varchar(300)")

                                }
                    }
        }