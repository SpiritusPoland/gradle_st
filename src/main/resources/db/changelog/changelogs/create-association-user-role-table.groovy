package db.changelog.changelogs


databaseChangeLog
        {
            changeSet(author: "LkMa", id:"create-association-user-role-table")
                    {
                        createTable(tableName:"user_role")
                                {
                                    column(name: "user_id", type: "Int", autoIncrement: true)
                                            {
                                                constraints(nullable: false, foreignKeyName:"user_id_FK", references: "user(user_id)" )
                                                //constraints(nullable: false,foreignKeyName: "userIdFK", references: "user(user_id)" )

                                            }
                                    column(name: "role_id", type: "Int")
                                            {
                                                constraints(nullable: false,  foreignKeyName: "role_id_FK", references: "role(role_id)")
                                            }
                                }
                        addPrimaryKey(tableName: "user_role", columnNames: "user_id, role_id", constraintName:"PK_user_role")
                    }
        }