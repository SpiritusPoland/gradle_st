package db.changelog.changelogs


databaseChangeLog
        {
            changeSet(author: "LkMa", id: "create-stage-table")
                    {
                        createTable(tableName: "stage")
                                {
                                    column(name: "stage_id", type: "Int", autoIncrement: true)
                                            {
                                                constraints(nullable: false, primaryKey: true)
                                            }
                                    column(name: "story_id", type: "Int")
                                    {
                                        constraints(nullable: false,foreignKeyName: "parent_story_id_FK", references: "story(story_id)")
                                    }
                                    column(name: "latitude", type: "double")
                                            {
                                                constraints(nullable: false)
                                            }
                                    column(name: "longitude", type: "double")
                                            {
                                                constraints(nullable: false)
                                            }
                                    column(name: "description", type: "TEXT")
                                    column(name: "photo_url", type: "varchar(300)")
                                    column(name: "distance_tolerance", type: "double", defaultValue: "5")
                                }
                    }
        }