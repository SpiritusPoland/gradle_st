package db.changelog.changelogs


databaseChangeLog
        {
            changeSet(author: "LkMa", id:"create-story-table")
                    {
                        createTable(tableName:"story")
                                {
                                    column(name: "story_id", type: "Int", autoIncrement: true)
                                            {
                                                constraints(nullable: false, primaryKey: true )
                                            }
                                    column(name: "story_name", type: "varchar(80)")
                                    column(name: "creation_timestamp", type: "timestamp", defaultValue:"now()")
                                    column(name: "user_id", type: "int")
                                            {
                                                constraints(nullable: false,  foreignKeyName: "creator_id_FK", references: "user(user_id)")
                                            }
                                    column(name: "description", type: "TEXT")
                                    column(name: "enabled", type: "tinyint", defaultValue: "0")
                                    column(name:"rating", type: "double")

                                }
                    }
        }