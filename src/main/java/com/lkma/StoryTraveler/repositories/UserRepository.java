package com.lkma.StoryTraveler.repositories;

import com.lkma.StoryTraveler.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
