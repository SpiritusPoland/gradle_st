package com.lkma.StoryTraveler.repositories;

import com.lkma.StoryTraveler.entities.Story;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoryRepository extends JpaRepository<Story,Long> {
}
