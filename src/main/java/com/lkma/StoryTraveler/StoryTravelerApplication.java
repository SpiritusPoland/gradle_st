package com.lkma.StoryTraveler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoryTravelerApplication {

	public static void main(String[] args) {
		SpringApplication.run(StoryTravelerApplication.class, args);
	}

}
