package com.lkma.StoryTraveler.entities;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
public class GameStage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private Long gameStageId;


    private Timestamp startTimestamp;
    private Timestamp endTimestamp;

    @OneToOne
    @JoinColumn(name="child_game_stage")
    private GameStage parentGameStage;

    @OneToOne
    @JoinColumn(name="parent_game_stage")
    private GameStage childGameStage;

    @ManyToOne
    @JoinColumn(name="base_stage")
    private Stage baseStage;
}
