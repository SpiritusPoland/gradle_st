package com.lkma.StoryTraveler.entities;

import javax.persistence.*;

@Entity
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ratingId;

    @ManyToOne
    @JoinColumn(name="story_id")
    Story story;

    @ManyToOne
    @JoinColumn(name = "user_id")
    User user;

    private double ratingScore;

    private String description;


}
