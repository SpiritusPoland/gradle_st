package com.lkma.StoryTraveler.entities;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Data
public class Story {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long storyId;
    private String storyName;
    private String description;
    private boolean enabled;
    private float rating;
    private Timestamp creationTimestamp;

    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    //List of Story Stages
    @OneToMany
    @JoinColumn(name="stage_id")
    private List<Stage> stageList;

    //List of Categories
    @ManyToMany
    @JoinTable(
            name = "story_category",
            joinColumns = @JoinColumn(name = "story_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id"))
    private List<Category> typeList;

    //list of Ratings
    @OneToMany
    private List<Rating> ratingList;


}
