package com.lkma.StoryTraveler.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Stage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stage_id")
    private Long stageId;

    private double latitude;
    private double longitude;

    private String description;
    //private String title;

    private String photoUrl;

    private double distanceTolerance;

    @ManyToOne
    @JoinTable(
            name = "stage_stage",
            joinColumns = @JoinColumn(name = "child_stage_id"),
            inverseJoinColumns = @JoinColumn(name = "parent_stage_id"))
    private Stage previousStage;

    @OneToMany
    @JoinTable(
            name = "stage_stage",
            joinColumns = @JoinColumn(name = "parent_stage_id"),
            inverseJoinColumns = @JoinColumn(name = "child_stage_id"))
    private List<Stage> nextStages;

    @OneToMany(mappedBy = "gameStageId")
    private List<GameStage> gameStageList;



}
