package com.lkma.StoryTraveler.entities;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long gameId;

    @ManyToOne
    @JoinColumn(name="story_id")
    private Story story;

    Timestamp startTimestamp;
    Timestamp endTimestamp;
}
