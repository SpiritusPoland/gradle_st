package com.lkma.StoryTraveler.entities;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Data
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;
    private String user;
    private String email;
    private String provider;
    private String provider_id;
    private Timestamp creationTimestamp;
    private Timestamp deletionTimestamp;


    //roles
    @ManyToMany
    @JoinTable(
            name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roleList;

    //played games

    @OneToMany
    @JoinColumn(name = "user_id")
    private List<Game> gameList;

    //created Stories

    @OneToMany
    @JoinColumn(name= "user_id")
    private List<Story> stories;

    @OneToMany
    @JoinColumn(name="user_id")
    private List<Rating> ratings;

}

